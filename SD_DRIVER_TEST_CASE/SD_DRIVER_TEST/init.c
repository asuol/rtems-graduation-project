#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <bsp.h> /* for device driver prototypes */
#include <emmc.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <rtems.h>
#include <rtems/test.h>

#include <rtems/bdpart.h>
#include <rtems/dosfs.h>
#include <rtems/fsmount.h>
#include <rtems/status-checks.h>
#include <errno.h>

/* forward declarations to avoid warnings */
rtems_task Init(rtems_task_argument argument);

const char rtems_test_name[] = "SD_DRIVER_TEST";

rtems_task Init(rtems_task_argument ignored)
{
  rtems_status_code sc;

  rtems_bdpart_format format;
  rtems_bdpart_partition pt [RTEMS_BDPART_PARTITION_NUMBER_HINT];
  size_t count = RTEMS_BDPART_PARTITION_NUMBER_HINT;

  int i;
  int rv = 0;

  char buffer[100];
  int fd, status;

  fstab_t fs_table[] = 
  {
    {
      "/dev/sd-card1","/mnt/hda", RTEMS_FILESYSTEM_TYPE_DOSFS,
      RTEMS_FILESYSTEM_READ_WRITE,
      FSMOUNT_MNT_OK | FSMOUNT_MNTPNT_CRTERR | FSMOUNT_MNT_FAILED,
      0
    }
  };

  memset(buffer, 0, sizeof(buffer));

  rtems_test_begin();

  /* Read partitions */
  sc = rtems_bdpart_read("/dev/sd-card", &format, pt, &count);

  /* Register partitions */
  rtems_bdpart_register("/dev/sd-card", pt, count);

  if(access("/dev/sd-card1", F_OK) != 0)
    printf ("Could not read the card's partitions...\n\n");

  if (rtems_fsmount( fs_table, sizeof(fs_table)/sizeof(fs_table[0]), NULL) != 0)
    printf ("Could not mount the card...");

  if(access("/mnt/hda", F_OK) != 0)
    printf("Error creating the card's mount point...\n\n");
  
  if(access("/mnt/hda/emmc", F_OK) != 0)
    printf("The file \"emmc\" does not exist in the card\n\n");
  
  fd = open("/mnt/hda/emmc" ,O_RDWR | O_APPEND);

  if (fd < 0) {
    printf("Error opening the file \"emmc\" from the card...\n\n");
    return;
  }

  read(fd, buffer, 100);

  printf("\nemmc file contents -> %s\n", buffer);
  
  if(write(fd, "This message confirms that the driver can write to the card", 59) != 59)
    printf("Could not write to the card\n\n");

  status = close (fd);
  if(access("/mnt/hda/andre", F_OK) == 0)
    printf("\"andre\" file already exists on the card\n\n");

  fd = open("/mnt/hda/andre", O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

  if (fd < 0) {

    printf("Error creating the file \"andre\" on the card...\n\n");
    return;
  }

  else
    printf("\"andre\" file successfully created\n\n");
  
  if(write(fd, "This message confirms that the driver can write to the card", 59) != 59)
    printf("Could not write to the card\n\n");
  
  status = close (fd);

  if(access("/mnt/hda/andre", F_OK) == 0)
    printf("\"andre\" file successfully written to the card\n\n");

  else
    printf("error writting \"andre\" file to the card...\n\n");

  if (unmount("/mnt/hda") == 0)
    printf("Card successfully unmounted\n\n");

  else
    printf("Error unmounting the card...\n\n");

  rtems_test_end();
  exit( 0 );
}

#define CONFIGURE_MAXIMUM_DRIVERS 10
#define CONFIGURE_APPLICATION_DOES_NOT_NEED_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_APPLICATION_EXTRA_DRIVERS SD_CARD_DRIVER_TABLE_ENTRY

#define CONFIGURE_APPLICATION_NEEDS_LIBBLOCK
#define CONFIGURE_FILESYSTEM_DOSFS

#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 30

#define CONFIGURE_MAXIMUM_TASKS 20

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

 #define CONFIGURE_INIT_TASK_STACK_SIZE (32 * 1024)

#define CONFIGURE_INITIAL_EXTENSIONS RTEMS_TEST_INITIAL_EXTENSION

#define CONFIGURE_INIT

#include <rtems/confdefs.h>
