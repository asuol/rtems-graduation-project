#ifndef TIMER_H
#define TIMER_H

#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>

struct timer_wait
{
	uint32_t trigger_value;
	int rollover;
};

int usleep(useconds_t usec);
struct timer_wait register_timer(useconds_t usec);
int compare_timer(struct timer_wait tw);

#define TIMEOUT_WAIT(stop_if_true, usec) 		\
do {							\
	struct timer_wait tw = register_timer(usec);	\
	do						\
	{						\
		if(stop_if_true)			\
			break;				\
	} while(!compare_timer(tw));			\
} while(0);

#endif

