/* 
 * References:
 *
 * https://github.com/raspberrypi/firmware/wiki/Mailboxes
 *
 * https://github.com/raspberrypi/firmware/wiki/Accessing-mailboxes
 */

#include <stdint.h>

#include <bsp/raspberrypi.h>
#include <bsp/mbox.h>

uint32_t mbox_read ( uint8_t channel )
{
    uint32_t data;
    uint8_t read_channel;

    while ( 1 )
    {
        while ( BCM2835_REG ( BCM2835_MBOX_STATUS ) & BCM2835_MBOX_EMPTY );

        data = BCM2835_REG ( BCM2835_MBOX_READ );
        read_channel = ( uint8_t ) ( data & 0xF );

        if ( read_channel == channel )
            return ( data & 0xFFFFFFF0 );
    }
}

void mbox_write( uint8_t channel, uint32_t data )
{
    while ( BCM2835_REG ( BCM2835_MBOX_STATUS ) & BCM2835_MBOX_FULL );

    BCM2835_REG (BCM2835_MBOX_WRITE) = ( data & 0xFFFFFFF0 ) | (uint32_t) ( channel & 0xF );
}
