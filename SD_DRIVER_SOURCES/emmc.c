#include <bsp/raspberrypi.h>
#include <bsp/mbox.h>

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <rtems/blkdev.h>
#include <rtems/diskdevs.h>

#include <rtems.h>

#include "emmc.h"
#include "timer_emmc.h"

#ifndef RTEMS_STATUS_CHECKS_USE_PRINTK
    #define RTEMS_STATUS_CHECKS_USE_PRINTK
#endif

#include <rtems/status-checks.h>

#define CSD_OFFSET 8

#define MEMORY_BARRIER()   \
    __asm__ ("mov r0, #0;" \
             "mcr p15, #0, r0, c7, c10, #5;" \
             "mov pc, lr;" )

/* Support for unaligned data access */
static inline void write_uint32 ( uint32_t v, uint8_t *s )
{
        *s++ = (uint8_t) (v);
	*s++ = (uint8_t) (v >> 8);
	*s++ = (uint8_t) (v >> 16);
	*s   = (uint8_t) (v >> 24);
}

static inline uint32_t read_uint32( const uint8_t *s )
{
  return (uint32_t) s [0] | ((uint32_t) s [1] << 8) | ((uint32_t) s [2] << 16) | ((uint32_t) s [3] << 24);
}

/* SD clock identification frequency (400 kHz) */
#define SD_CLOCK_ID    400000
#define SD_CLOCK_NORMAL     25000000

#define SD_CARD_INTERRUPT    (1 << 8)

static uint32_t hci_ver = 0;

#define SD_CMD_INDEX(a)          ((a) << 24)
#define SD_APP_CMD_INDEX(a)      ((a) << 24)
#define SD_CMD_TYPE_NORMAL       0x0
#define SD_CMD_TYPE_SUSPEND      (1 << 22)
#define SD_CMD_TYPE_RESUME       (2 << 22)
#define SD_CMD_TYPE_ABORT        (3 << 22)
#define SD_CMD_TYPE_MASK         (3 << 22)
#define SD_CMD_ISDATA            (1 << 21)
#define SD_CMD_IXCHK_EN          (1 << 20)
#define SD_CMD_CRCCHK_EN         (1 << 19)
#define SD_CMD_RSPNS_TYPE_NONE   0                       // For no response
#define SD_CMD_RSPNS_TYPE_136    (1 << 16)               // For response R2 (with CRC), R3,4 (no CRC)
#define SD_CMD_RSPNS_TYPE_48     (2 << 16)               // For responses R1, R5, R6, R7 (with CRC)
#define SD_CMD_RSPNS_TYPE_48B    (3 << 16)               // For responses R1b, R5b (with CRC)
#define SD_CMD_RSPNS_TYPE_MASK   (3 << 16)               // CMDTM register. 48B = 48bits with busy
#define SD_CMD_MULTI_BLOCK       (1 << 5)
#define SD_CMD_DAT_DIR_HC        0
#define SD_CMD_DAT_DIR_CH        (1 << 4)
#define SD_CMD_AUTO_CMD_EN_NONE  0
#define SD_CMD_AUTO_CMD_EN_CMD12 (1 << 2)
#define SD_CMD_AUTO_CMD_EN_CMD23 (2 << 2)
#define SD_CMD_BLKCNT_EN         (1 << 1)
#define SD_CMD_DMA               1

#define SD_ERR_CMD_TIMEOUT       0
#define SD_ERR_CMD_CRC           1
#define SD_ERR_CMD_END_BIT       2
#define SD_ERR_CMD_INDEX         3
#define SD_ERR_DATA_TIMEOUT      4
#define SD_ERR_DATA_CRC          5
#define SD_ERR_DATA_END_BIT      6
#define SD_ERR_CURRENT_LIMIT     7
#define SD_ERR_AUTO_CMD12        8
#define SD_ERR_ADMA              9
#define SD_ERR_TUNING            10
#define SD_ERR_RSVD              11

#define SD_ERR_MASK_CMD_TIMEOUT         (1 << (16 + SD_ERR_CMD_TIMEOUT))
#define SD_ERR_MASK_CMD_CRC             (1 << (16 + SD_ERR_CMD_CRC))
#define SD_ERR_MASK_CMD_END_BIT         (1 << (16 + SD_ERR_CMD_END_BIT))
#define SD_ERR_MASK_CMD_INDEX           (1 << (16 + SD_ERR_CMD_INDEX))
#define SD_ERR_MASK_DATA_TIMEOUT        (1 << (16 + SD_ERR_CMD_TIMEOUT))
#define SD_ERR_MASK_DATA_CRC            (1 << (16 + SD_ERR_CMD_CRC))
#define SD_ERR_MASK_DATA_END_BIT        (1 << (16 + SD_ERR_CMD_END_BIT))
#define SD_ERR_MASK_CURRENT_LIMIT       (1 << (16 + SD_ERR_CMD_CURRENT_LIMIT))
#define SD_ERR_MASK_AUTO_CMD12          (1 << (16 + SD_ERR_CMD_AUTO_CMD12))
#define SD_ERR_MASK_ADMA                (1 << (16 + SD_ERR_CMD_ADMA))
#define SD_ERR_MASK_TUNING              (1 << (16 + SD_ERR_CMD_TUNING))

#define SD_COMMAND_COMPLETE     1
#define SD_TRANSFER_COMPLETE    (1 << 1)
#define SD_BLOCK_GAP_EVENT      (1 << 2)
#define SD_DMA_INTERRUPT        (1 << 3)
#define SD_BUFFER_WRITE_READY   (1 << 4)
#define SD_BUFFER_READ_READY    (1 << 5)
#define SD_CARD_INSERTION       (1 << 6)
#define SD_CARD_REMOVAL         (1 << 7)
#define SD_CARD_INTERRUPT       (1 << 8)

#define SD_RESP_NONE        SD_CMD_RSPNS_TYPE_NONE
#define SD_RESP_R1          (SD_CMD_RSPNS_TYPE_48 | SD_CMD_CRCCHK_EN)
#define SD_RESP_R1b         (SD_CMD_RSPNS_TYPE_48B | SD_CMD_CRCCHK_EN)
#define SD_RESP_R2          (SD_CMD_RSPNS_TYPE_136 | SD_CMD_CRCCHK_EN)
#define SD_RESP_R3          SD_CMD_RSPNS_TYPE_48
#define SD_RESP_R4          SD_CMD_RSPNS_TYPE_136
#define SD_RESP_R5          (SD_CMD_RSPNS_TYPE_48 | SD_CMD_CRCCHK_EN)
#define SD_RESP_R5b         (SD_CMD_RSPNS_TYPE_48B | SD_CMD_CRCCHK_EN)
#define SD_RESP_R6          (SD_CMD_RSPNS_TYPE_48 | SD_CMD_CRCCHK_EN)
#define SD_RESP_R7          (SD_CMD_RSPNS_TYPE_48 | SD_CMD_CRCCHK_EN)

#define SD_DATA_READ        (SD_CMD_ISDATA | SD_CMD_DAT_DIR_CH)
#define SD_DATA_WRITE       (SD_CMD_ISDATA | SD_CMD_DAT_DIR_HC)

#define SD_CMD_RESERVED(a)  0xffffffff

#define SUCCESS(a)          (a->last_cmd_success)
#define FAIL(a)             (a->last_cmd_success == 0)
#define TIMEOUT(a)          (FAIL(a) && (a->last_error == 0))
#define CMD_TIMEOUT(a)      (FAIL(a) && (a->last_error & (1 << 16)))
#define CMD_CRC(a)          (FAIL(a) && (a->last_error & (1 << 17)))
#define CMD_END_BIT(a)      (FAIL(a) && (a->last_error & (1 << 18)))
#define CMD_INDEX(a)        (FAIL(a) && (a->last_error & (1 << 19)))
#define DATA_TIMEOUT(a)     (FAIL(a) && (a->last_error & (1 << 20)))
#define DATA_CRC(a)         (FAIL(a) && (a->last_error & (1 << 21)))
#define DATA_END_BIT(a)     (FAIL(a) && (a->last_error & (1 << 22)))
#define CURRENT_LIMIT(a)    (FAIL(a) && (a->last_error & (1 << 23)))
#define ACMD12_ERROR(a)     (FAIL(a) && (a->last_error & (1 << 24)))
#define ADMA_ERROR(a)       (FAIL(a) && (a->last_error & (1 << 25)))
#define TUNING_ERROR(a)     (FAIL(a) && (a->last_error & (1 << 26)))

static struct emmc_block_dev sd_card_driver_table [1];

enum commands {
    SD_CMD_GO_IDLE_STATE          = SD_CMD_INDEX(0),
    SD_CMD_ALL_SEND_CID           = SD_CMD_INDEX(2) | SD_RESP_R2,
    SD_CMD_SEND_RELATIVE_ADDR     = SD_CMD_INDEX(3) | SD_RESP_R6,
    SD_CMD_IO_SET_OP_COND         = SD_CMD_INDEX(5) | SD_RESP_R4,
    SD_CMD_SELECT_DESELECT_CARD   = SD_CMD_INDEX(7) | SD_RESP_R1b,
    SD_CMD_SEND_IF_COND           = SD_CMD_INDEX(8) | SD_RESP_R7,
    SD_CMD_SEND_CSD               = SD_CMD_INDEX(9) | SD_RESP_R2,
    SD_CMD_VOLTAGE_SWITCH         = SD_CMD_INDEX(11) | SD_RESP_R1,
    SD_CMD_STOP_TRANSMISSION      = SD_CMD_INDEX(12) | SD_RESP_R1b | SD_CMD_TYPE_ABORT,
    SD_CMD_SEND_STATUS            = SD_CMD_INDEX(13) | SD_RESP_R1,
    SD_CMD_SET_BLOCKLEN           = SD_CMD_INDEX(16) | SD_RESP_R1,
    SD_CMD_READ_SINGLE_BLOCK      = SD_CMD_INDEX(17) | SD_RESP_R1 | SD_DATA_READ,
    SD_CMD_READ_MULTIPLE_BLOCK    = SD_CMD_INDEX(18) | SD_RESP_R1 | SD_DATA_READ | SD_CMD_MULTI_BLOCK | SD_CMD_BLKCNT_EN,
    SD_CMD_WRITE_BLOCK            = SD_CMD_INDEX(24) | SD_RESP_R1 | SD_DATA_WRITE,
    SD_CMD_WRITE_MULTIPLE_BLOCK   = SD_CMD_INDEX(25) | SD_RESP_R1 | SD_DATA_WRITE | SD_CMD_MULTI_BLOCK | SD_CMD_BLKCNT_EN,
    SD_CMD_APP_CMD                = SD_CMD_INDEX(55) | SD_RESP_R1,
    APP_CMD_SET_BUS_WIDTH          = SD_APP_CMD_INDEX(6) | SD_RESP_R1,
    APP_CMD_SEND_OP_COND           = SD_APP_CMD_INDEX(41) | SD_RESP_R3,
    APP_CMD_SEND_SCR               = SD_APP_CMD_INDEX(51) | SD_RESP_R1 | SD_DATA_READ
};

#define SD_RESET_CMD            (1 << 25)
#define SD_RESET_DAT            (1 << 26)
#define SD_RESET_ALL            (1 << 24)

static char *err_irpts[] = { "CMD_TIMEOUT", "CMD_CRC", "CMD_END_BIT", "CMD_INDEX",
        "DATA_TIMEOUT", "DATA_CRC", "DATA_END_BIT", "CURRENT_LIMIT",
        "AUTO_CMD12", "ADMA", "TUNING", "RSVD" };

/* Get the base clock rate */
static uint32_t sd_get_base_clock_hz ( struct emmc_block_dev *e )
{
    uint32_t base_clock;
    
    volatile uint32_t *mailbuffer;

    /* Set up the buffer */
    mailbuffer[0] = 8 * 4;              /* size of this message */
    mailbuffer[1] = 0;                  /* this is a request */

    /* next comes the first tag */
    mailbuffer[2] = 0x00030002;         /* get clock rate tag */
    mailbuffer[3] = 0x8;                /* value buffer size */
    mailbuffer[4] = 0x4;                /* is a request, value length = 4 */
    mailbuffer[5] = 0x1;                /* clock id + space to return clock id */
    mailbuffer[6] = 0;                  /* space to return rate (in Hz) */

    /* closing tag */
    mailbuffer[7] = 0;

    /* send the message */
    mbox_write (BCM2835_MBOX_CHANNEL_PROP, mailbuffer);

    /* read the response */
    mbox_read (BCM2835_MBOX_CHANNEL_PROP);

    if(mailbuffer[1] != BCM2835_MBOX_SUCCESS)
    {
        if (e->verbose)
            printk ("EMMC: property mailbox did not return a valid response.\n");

        return 0;
    }

    if (mailbuffer[5] != 0x1)
    {
        if (e->verbose)
            printk ("EMMC: property mailbox did not return a valid clock id.\n");

        return 0;
    }

    base_clock = mailbuffer[6];

    if (e->verbose)
        printk ("EMMC: base clock rate is %i Hz\n", base_clock);

    return base_clock;
}

/* Set the clock dividers to generate a target value */
static rtems_status_code sd_get_clock_divider ( struct emmc_block_dev *e, uint32_t* divider, uint32_t base_clock, uint32_t target_rate )
{
    uint32_t targetted_divisor = 0;

    if (target_rate > base_clock)
        targetted_divisor = 1;

    else
    {
        targetted_divisor = base_clock / target_rate;

        uint32_t mod = base_clock % target_rate;

        if (mod)
            targetted_divisor--;
    }

    /* Decide on the clock mode to use 
     * Currently only 10-bit divided clock mode is supported 
     */

    if (hci_ver >= 2)
    {
        /* HCI version 3 or greater supports 10-bit divided clock mode 
         * This requires a power-of-two divider
         *
         * Find the first bit set 
         */
        int divisor = -1;

        int first_bit;

        for (first_bit = 31; first_bit >= 0; first_bit--)
        {
            uint32_t bit_test = (1 << first_bit);

            if (targetted_divisor & bit_test)
            {
                divisor = first_bit;
                targetted_divisor &= ~bit_test;

                if (targetted_divisor)
                {
                    /* The divisor is not a power-of-two, increase it */
                    divisor++;
                }

                break;
            }
        }

        if (divisor == -1)
            divisor = 31;

        if (divisor >= 32)
            divisor = 31;

        if (divisor != 0)
            divisor = (1 << (divisor - 1));

        if (divisor >= 0x400)
            divisor = 0x3ff;

        uint32_t freq_select = divisor & 0xff;
        uint32_t upper_bits = (divisor >> 8) & 0x3;

        *divider = (freq_select << 8) | (upper_bits << 6) | (0 << 5);

        if (e->verbose)
	{
            int denominator = 1;

            if(divisor != 0)
                denominator = divisor * 2;

            int actual_clock = base_clock / denominator;

            printk ("EMMC: base_clock: %i, target_rate: %i, divisor: %08x, "
                    "actual_clock: %i, ret: %08x\n", base_clock, target_rate,
	                                             divisor, actual_clock, *divider);
        }

        return RTEMS_SUCCESSFUL;
    }

    else
    {
        printk ("EMMC: unable to get a valid clock divider for ID frequency\n");
        return RTEMS_UNSATISFIED;
    }
}

/* Reset the CMD line */
static int sd_reset_cmd ()
{
    uint32_t control1 = BCM2835_REG (BCM2835_EMMC_CONTROL1);
    control1 |= SD_RESET_CMD;

    BCM2835_REG (BCM2835_EMMC_CONTROL1) = control1;

    TIMEOUT_WAIT((BCM2835_REG (BCM2835_EMMC_CONTROL1) & SD_RESET_CMD) == 0, 1000000);

    if ((BCM2835_REG (BCM2835_EMMC_CONTROL1) & SD_RESET_CMD) != 0)
    {
        printk ("EMMC: CMD line did not reset properly\n");
        return -1;
    }
        
    return 0;
}

/* Switch the clock rate whilst running */
static int sd_switch_clock_rate ( struct emmc_block_dev *e, uint32_t base_clock, uint32_t target_rate )
{
    /* Decide on an appropriate divider */
    rtems_status_code sc;
    uint32_t divider;

    sc = sd_get_clock_divider (e, &divider, base_clock, target_rate);
    RTEMS_CHECK_SC( sc, "EMMC: Get Clock Divider");

    /* Wait for the command inhibit (CMD and DAT) bits to clear */
    while (BCM2835_REG (BCM2835_EMMC_STATUS) & 0x3)
        usleep(1000);

    /* Set the SD clock off */
    uint32_t control1 = BCM2835_REG (BCM2835_EMMC_CONTROL1);
    control1 &= ~(1 << 2);

    BCM2835_REG (BCM2835_EMMC_CONTROL1) = control1;
    usleep(2000);

    /* Write the new divider */
    control1 &= ~0xffe0;                /* Clear old setting + clock generator select */
    control1 |= divider;

    BCM2835_REG (BCM2835_EMMC_CONTROL1) = control1;
    usleep(2000);

    /* Enable the SD clock */
    control1 |= (1 << 2);

    BCM2835_REG (BCM2835_EMMC_CONTROL1) = control1;

    usleep(2000);

    if (e->verbose)
        printk("EMMC: successfully set clock rate to %i Hz\n", target_rate);

    return 0;
}

/* Reset the CMD line */
static int sd_reset_dat()
{
    uint32_t control1 = BCM2835_REG (BCM2835_EMMC_CONTROL1);
    control1 |= SD_RESET_DAT;

    BCM2835_REG (BCM2835_EMMC_CONTROL1) = control1;
        
    TIMEOUT_WAIT ((BCM2835_REG (BCM2835_EMMC_CONTROL1) & SD_RESET_DAT) == 0, 1000000);
        
    if ((BCM2835_REG (BCM2835_EMMC_CONTROL1) & SD_RESET_DAT) != 0)
    {
        printk ("EMMC: DAT line did not reset properly\n");
        return -1;
    }

    return 0;
}

static rtems_status_code sd_issue_command_int ( struct emmc_block_dev *e, enum commands cmd_reg, uint32_t argument, useconds_t timeout )
{
    e->last_cmd_reg = cmd_reg;
    e->last_cmd_success = 0;

    if (e->verbose)
        printk ("EMMC: issuing command\n\n");

    /* This is as per Host Controller Simplified Specification 3.7.1.1/3.7.2.2 */

    /* Check Command Inhibit */
    while (BCM2835_REG (BCM2835_EMMC_STATUS) & BCM2835_BIT (0))
        usleep (1000);

    /* Has the command a busy signal? */
    if ((cmd_reg & SD_CMD_RSPNS_TYPE_MASK) == SD_CMD_RSPNS_TYPE_48B)
    {
        /* Is this not an abort command? */
        if ((cmd_reg & SD_CMD_TYPE_MASK) != SD_CMD_TYPE_ABORT)
        {
            /* Wait for the data line to be free */
            while (BCM2835_REG (BCM2835_EMMC_STATUS) & BCM2835_BIT (1))
                usleep (1000);
        }
    }

    /* Set block size and block count
     * For now, block size = 512 bytes, block count = 1
     */
    if (e->blocks_to_transfer > 0xffff)
    {
        printk ("EMMC: blocks_to_transfer too great (%i)\n"
               , e->blocks_to_transfer);

	if (e->verbose)
            printk("EMMC: error issuing command: interrupts %08x \n", e->last_interrupt);

        return RTEMS_TIMEOUT;
    }

    uint32_t blksizecnt = e->block_size | (e->blocks_to_transfer << 16);
    BCM2835_REG (BCM2835_EMMC_BLKSIZECNT) = blksizecnt;

    /* Set argument 1 reg */
    BCM2835_REG (BCM2835_EMMC_ARG1) = argument;

    /* Issue the SD command */
    BCM2835_REG (BCM2835_EMMC_CMDTM) = cmd_reg;

    usleep (2000);

    /* Wait for command complete interrupt */
    TIMEOUT_WAIT (BCM2835_REG (BCM2835_EMMC_INTERRUPT) & 0x8001, timeout);

    uint32_t irpts = BCM2835_REG (BCM2835_EMMC_INTERRUPT);

    /* Clear command complete status */
    BCM2835_REG (BCM2835_EMMC_INTERRUPT) = 0xFFFF0001;

    /* Test for errors */
    if ((irpts & 0xFFFF0001) != 0x1)
    {
        if (e->verbose)
            printk ("EMMC: error occured whilst waiting for command complete interrupt\n");

        e->last_error = irpts & 0xffff0000;
        e->last_interrupt = irpts;
	
	goto error;
    }

    usleep (2000);

    /* Get response data */
    switch (cmd_reg & SD_CMD_RSPNS_TYPE_MASK)
    {
        case SD_CMD_RSPNS_TYPE_48:
        case SD_CMD_RSPNS_TYPE_48B:
            e->last_r0 = BCM2835_REG (BCM2835_EMMC_RESP0);
            break;

        case SD_CMD_RSPNS_TYPE_136:
            e->last_r0 = BCM2835_REG (BCM2835_EMMC_RESP0);
            e->last_r1 = BCM2835_REG (BCM2835_EMMC_RESP1);
            e->last_r2 = BCM2835_REG (BCM2835_EMMC_RESP2);
            e->last_r3 = BCM2835_REG (BCM2835_EMMC_RESP3);
            break;
    }

    /* If with data, wait for the appropriate interrupt */
    if ((cmd_reg & SD_CMD_ISDATA))
    {
        uint32_t wr_irpt;
        int is_write = 0;

        if (cmd_reg & SD_CMD_DAT_DIR_CH)
            wr_irpt = (1 << 5);     /* read */
        else
        {
            is_write = 1;
            wr_irpt = (1 << 4);     /* write */
        }

        int cur_block = 0;
        uint32_t *cur_buf_addr = (uint32_t *)e->buf;

        while (cur_block < e->blocks_to_transfer)
        {
            if (e->verbose)
	    {
                if (e->blocks_to_transfer > 1)
                    printk("EMMC: multi block transfer, awaiting block %i ready\n", cur_block);
            }

            TIMEOUT_WAIT (BCM2835_REG (BCM2835_EMMC_INTERRUPT) & (wr_irpt | 0x8000), timeout);

            irpts = BCM2835_REG (BCM2835_EMMC_INTERRUPT);

            BCM2835_REG (BCM2835_EMMC_INTERRUPT) = 0xffff0000 | wr_irpt;

            if ((irpts & (0xffff0000 | wr_irpt)) != wr_irpt)
            {
                if (e->verbose)
                    printk("EMMC: error occured whilst waiting for data ready interrupt\n");

                e->last_error = irpts & 0xffff0000;
                e->last_interrupt = irpts;
		
		goto error;
            }

            /* Transfer the block */
            size_t cur_byte_no = 0;

	    uint32_t data;

            while (cur_byte_no < e->block_size)
            {
                if (is_write)
                {
                    data = read_uint32 ((uint8_t *)cur_buf_addr);
                    BCM2835_REG (BCM2835_EMMC_DATA) = data;
                }

                else
                {
                    data = BCM2835_REG (BCM2835_EMMC_DATA);
		    write_uint32 (data, (uint8_t *)cur_buf_addr);
                }

                cur_byte_no += 4;
                cur_buf_addr++;
            }

            if (e->verbose)
                printk("EMMC: block %i transfer complete\n", cur_block);

            cur_block++;
        }
    }

    /* Wait for transfer complete (set if read/write transfer or with busy) */
    if ((((cmd_reg & SD_CMD_RSPNS_TYPE_MASK) == SD_CMD_RSPNS_TYPE_48B) ||
        (cmd_reg & SD_CMD_ISDATA)))
    {
        /* First check command inhibit (DAT) is not already 0 */
        if ((BCM2835_REG (BCM2835_EMMC_STATUS) & 0x2) == 0)
            BCM2835_REG (BCM2835_EMMC_INTERRUPT) = 0xffff0002;

        else
        {
            TIMEOUT_WAIT (BCM2835_REG (BCM2835_EMMC_INTERRUPT) & 0x8002, timeout);

            irpts = BCM2835_REG (BCM2835_EMMC_INTERRUPT);

            BCM2835_REG (BCM2835_EMMC_INTERRUPT) = 0xffff0002;

            /* Handle the case where both data timeout and transfer complete
             * are set - transfer complete overrides data timeout: HCSS 2.2.17
             */
            if (((irpts & 0xffff0002) != 0x2) && ((irpts & 0xffff0002) != 0x100002))
            {
                if (e->verbose)
                    printk("EMMC: error occured whilst waiting for transfer complete interrupt\n");

                e->last_error = irpts & 0xffff0000;
                e->last_interrupt = irpts;

                goto error;
            }

            BCM2835_REG (BCM2835_EMMC_INTERRUPT) = 0xffff0002;
        }
    }
    
    if (e->verbose)
        printk("EMMC: command completed successfully\n");

    return RTEMS_SUCCESSFUL;

 error:

    if (TIMEOUT(e))
      return RTEMS_TIMEOUT;

    if (CMD_TIMEOUT (e))
    {
        if (sd_reset_cmd () == -1)
            return RTEMS_UNSATISFIED;

        BCM2835_REG (BCM2835_EMMC_INTERRUPT) = SD_ERR_MASK_CMD_TIMEOUT;

	return RTEMS_TIMEOUT;
    }

    if (e->verbose)
	  {
                printk("EMMC: error issuing command: interrupts %08x: ", e->last_interrupt);
	    
	        int i;

                for(i = 0; i < SD_ERR_RSVD; i++)
                {
                    if(e->last_error & (1 << (i + 16)))
                    {
                        printk(err_irpts[i]);
                        printk(" ");
                    }
                }

		printk("\n");
	  }

        return RTEMS_UNSATISFIED;
}

/* Handle a card interrupt */
static void sd_handle_card_interrupt ( struct emmc_block_dev *e )
{
    if (e->verbose)
    {
        uint32_t status = BCM2835_REG (BCM2835_EMMC_STATUS);

        printk ("EMMC: card interrupt\n");
        printk ("EMMC: controller status: %08x\n", status);
    }

    /* Get the card status */
    if (e->card_rca)
    {
        sd_issue_command_int (e, SD_CMD_SEND_STATUS, e->card_rca << 16 ,500000);

        if (FAIL(e))
        {
            if (e->verbose)
                printk("EMMC: unable to get card status\n");
        }

        else
        {
            if (e->verbose)
                printk("EMMC: card status: %08x\n", e->last_r0);
        }
    }

    else
    {
        if (e->verbose)
            printk("EMMC: no card currently selected\n");
    }
}

static void sd_handle_interrupts ( struct emmc_block_dev *e )
{
    uint32_t irpts = BCM2835_REG (BCM2835_EMMC_INTERRUPT);
    uint32_t reset_mask = 0;

    if (irpts & SD_COMMAND_COMPLETE)
    {
        if (e->verbose)
            printk ("EMMC: spurious command complete interrupt\n");

        reset_mask |= SD_COMMAND_COMPLETE;
    }

    if (irpts & SD_TRANSFER_COMPLETE)
    {
        if (e->verbose)
            printk ("EMMC: spurious transfer complete interrupt\n");

        reset_mask |= SD_TRANSFER_COMPLETE;
    }

    if (irpts & SD_BLOCK_GAP_EVENT)
    {
        if (e->verbose)
            printk ("EMMC: spurious block gap event interrupt\n");

        reset_mask |= SD_BLOCK_GAP_EVENT;
    }

    if (irpts & SD_DMA_INTERRUPT)
    {
        if (e->verbose)
            printk ("EMMC: spurious DMA interrupt\n");

        reset_mask |= SD_DMA_INTERRUPT;
    }

    if (irpts & SD_BUFFER_WRITE_READY)
    {
        if (e->verbose)
            printk ("EMMC: spurious buffer write ready interrupt\n");

        reset_mask |= SD_BUFFER_WRITE_READY;

        sd_reset_dat ();
    }

    if (irpts & SD_BUFFER_READ_READY)
    {
        if (e->verbose)
            printk ("EMMC: spurious buffer read ready interrupt\n");

        reset_mask |= SD_BUFFER_READ_READY;

        sd_reset_dat ();
    }

    if (irpts & SD_CARD_INSERTION)
    {
        if (e->verbose)
            printk ("EMMC: card insertion detected\n");

        reset_mask |= SD_CARD_INSERTION;
    }

    if (irpts & SD_CARD_REMOVAL)
    {
        if (e->verbose)
            printk ("EMMC: card removal detected\n");

        reset_mask |= SD_CARD_REMOVAL;

        e->card_removal = 1;
    }

    if (irpts & SD_CARD_INTERRUPT)
    {
        if (e->verbose)
            printk ("EMMC: card interrupt detected\n");

        sd_handle_card_interrupt (e);

        reset_mask |= SD_CARD_INTERRUPT;
    }

    if (irpts & 0x8000)
    {
        if (e->verbose)
            printk ("EMMC: spurious error interrupt: %08x\n", irpts);

        reset_mask |= 0xffff0000;
    }

    BCM2835_REG (BCM2835_EMMC_INTERRUPT) = reset_mask;
}

static rtems_status_code sd_issue_command( struct emmc_block_dev *e, enum commands command, uint32_t argument, useconds_t timeout )
{
    /* First, handle any pending interrupts */
    sd_handle_interrupts(e);

    /* Stop the command issue if it was the card remove interrupt that was handled */
    if (e->card_removal)
    {
        e->last_cmd_success = 0;
        return RTEMS_UNSATISFIED;
    }
    
    return sd_issue_command_int (e, command, argument, timeout);
}

static int disk_ioctl(rtems_disk_device *dd, uint32_t req, void *arg)
{
    rtems_status_code sc = RTEMS_SUCCESSFUL;
    rtems_device_minor_number minor = rtems_disk_get_minor_number ( dd );
    
    struct emmc_block_dev *e;
    rtems_blkdev_request *r = arg;

    int rv = 0;

    e = &sd_card_driver_table [ minor ];
   
    if (req == RTEMS_BLKIO_REQUEST) {
        
        switch (r->req)
        {
            /* Read the requested blocks of data. Use block IO */
            case RTEMS_BLKDEV_REQ_READ:

	        rv = sd_read (e, r->bufs->buffer, r->bufs->length, r->bufs->block);

                if(rv < 0)
                    printk ("sd read fail\n\n");

                break;

            /* Write the requested blocks of data. Use block IO */
            case RTEMS_BLKDEV_REQ_WRITE:

                rv = sd_write (e, r->bufs->buffer, r->bufs->length, r->bufs->block);

                if(rv < 0)
                    printk ("sd write fail\n\n");

                break;

            /* Sync any data with the media. Use cache */
            case RTEMS_BLKDEV_REQ_SYNC:
                printk ("disk_ioctl -> Sync request\n\n");
                break;

            default:
                printk ("disk_ioctl -> Unkown request\n\n");
        }

        rtems_blkdev_request_done(r, sc);

        return 0;
    }

    else if (req == RTEMS_BLKIO_CAPABILITIES) 
    {
        *(uint32_t *) arg = RTEMS_BLKDEV_CAP_MULTISECTOR_CONT;
        return 0;
    }

    else 
        return rtems_blkdev_ioctl(dd, req, arg);
}

/*  
 *  Detects inserted or removed card 
 *  HCSS v3.00 page 92-93
 */
static rtems_status_code card_detection (struct emmc_block_dev *e)
{
    if (e->verbose)
        printk ("EMMC: checking for an inserted card\n");

    TIMEOUT_WAIT (BCM2835_REG (BCM2835_EMMC_STATUS) & (1 << 16), 500000);
    
    uint32_t status_reg = BCM2835_REG (BCM2835_EMMC_STATUS);
    
    if ((status_reg & (1 << 16)) == 0)
    {
        printk ("EMMC: no card inserted\n");
        return RTEMS_UNSATISFIED;
    }

    if (e->verbose)
        printk ("EMMC: status: %08x\n", status_reg);

    return RTEMS_SUCCESSFUL;
}

/*  
 *  Detects inserted or removed card 
 *  HCSS v3.00 page 96-97
 */

static rtems_status_code sd_card_init (struct emmc_block_dev *e )
{
    rtems_status_code sc;
   
    if (e->verbose)
        printk ("EMMC: sd_card_init started\n");

    /* Get SD card slot version */
    hci_ver = (BCM2835_REG (BCM2835_EMMC_SLOTISR_VER) >> 16) & 0xFF;

    if (hci_ver < 2)
    { 
        printk ("EMMC: only SDHCI versions >= 2.0 are supported\n");
        return RTEMS_UNSATISFIED;
    }

    /* Check for a valid card */
    sc = card_detection (e);
    RTEMS_CHECK_SC( sc, "EMMC: Card detection");

    /* Reset the controller */
    if (e->verbose)
        printk ("EMMC: resetting controller\n");

    uint32_t control1 = BCM2835_REG (BCM2835_EMMC_CONTROL1);

    /* Enable SRST_HC bit to reset complete host controller circuit */
    control1 |= (1 << 24);

    /* Disable SD clock */
    control1 &= ~(1 << 2);

    /* Disable internal EMMC clocks */
    control1 &= ~(1 << 0);

    BCM2835_REG (BCM2835_EMMC_CONTROL1) = control1;

    /* Check if SRST_HC, SRST_CMD and SRST_DATA reset bits are enabled */
    TIMEOUT_WAIT ((BCM2835_REG (BCM2835_EMMC_CONTROL1) & (0x7 << 24)) == 0, 1000000);

    if ((BCM2835_REG (BCM2835_EMMC_CONTROL1) & (0x7 << 24)) != 0)
    {
        printk ("EMMC: controller did not reset properly\n");
        return RTEMS_UNSATISFIED;
    }

    if (e->verbose)
    {
        printk ("EMMC: reset successful\n");

        printk ("EMMC: control0: %08x, control1: %08x, control2: %08x\n"
               , BCM2835_REG (BCM2835_EMMC_CONTROL0)
               , BCM2835_REG (BCM2835_EMMC_CONTROL1)
               , BCM2835_REG (BCM2835_EMMC_CONTROL2));
    }

    /*
     *  SD Clock Supply sequence
     *  HCSS v3.00 page 94
     */

    /* Get the base clock rate */
    uint32_t base_clock = sd_get_base_clock_hz (e);

    if (base_clock == 0)
    {
        if (e->verbose)
            printk ("EMMC: assuming clock rate to be 100MHz\n");

        base_clock = 100000000;
    }

    /* Set clock rate to something slow */
    if (e->verbose)
        printk ("EMMC: setting clock rate\n");

    /* enable CLK_INTLEN */
    control1 = BCM2835_REG (BCM2835_EMMC_CONTROL1);
    control1 |= 1;

    /* Set to identification frequency (400 kHz) */
    uint32_t f_id;

    sc = sd_get_clock_divider (e, &f_id, base_clock, SD_CLOCK_ID);
    RTEMS_CHECK_SC( sc, "EMMC: Get Clock Divider");
 
    control1 |= f_id;

    /* 
     * DATA_TOUNIT 
     * data timeout unit exponent = TMCLK * 2^10 
     */
    control1 |= (7 << 16);          

    BCM2835_REG (BCM2835_EMMC_CONTROL1) = control1;

    TIMEOUT_WAIT (BCM2835_REG (BCM2835_EMMC_CONTROL1) & 0x2, 0x1000000);

    /* Check if CLK_STABLE register is 1 */
    if ((BCM2835_REG (BCM2835_EMMC_CONTROL1) & 0x2) == 0)
    {
        printk ("EMMC: controller's clock did not stabilise within 1 second\n");
        return RTEMS_UNSATISFIED;
    }

    if (e->verbose)
    {
        printk ("EMMC: control0: %08x, control1: %08x\n"
               , BCM2835_REG (BCM2835_EMMC_CONTROL0)
               , BCM2835_REG (BCM2835_EMMC_CONTROL1));
    }

    /* Enable the SD clock */
    if (e->verbose)
        printk ("EMMC: enabling SD clock\n");

    usleep (2000);

    control1 = BCM2835_REG (BCM2835_EMMC_CONTROL1);
    control1 |= (1 << 2);

    BCM2835_REG (BCM2835_EMMC_CONTROL1) = control1;

    usleep (2000);

    if (e->verbose)
        printk ("EMMC: SD clock enabled\n");

    /* Mask off sending interrupts to the ARM */
    BCM2835_REG (BCM2835_EMMC_IRPT_EN) = 0;
    
    /* Reset interrupts */
    BCM2835_REG (BCM2835_EMMC_INTERRUPT) = 0xFFFFFFFF;
        
    /* Have all interrupts sent to the INTERRUPT register */
    uint32_t irpt_mask = 0xFFFFFFFF & (~SD_CARD_INTERRUPT);

    BCM2835_REG (BCM2835_EMMC_IRPT_MASK) = irpt_mask;

    if (e->verbose)
        printk ("EMMC: interrupts disabled\n");

    usleep (2000);

    e->base_clock = base_clock;

    /*
     *  Card Initialization and Identification Sequence
     *  HCSS v3.00 page 100 - 103
     */

    /* Send CMD0 to the card (reset to idle state) */
    sc = sd_issue_command (e, SD_CMD_GO_IDLE_STATE, 0, 500000);
    RTEMS_CHECK_SC( sc, "EMMC: no CMD0 response");

    /* 
     * Send CMD8 to the card
     * Voltage supplied = 0x1 = 2.7-3.6V (standard)
     * Check pattern = 10101010b (as recommended on PLSS v3.01) = 0xAA
     */
    if (e->verbose)
        printk ("EMMC: note a timeout error on the following command (CMD8) is normal "
                "and expected if the SD card version is less than 2.0\n");

    sc = sd_issue_command (e, SD_CMD_SEND_IF_COND, 0x1AA, 500000);
    RTEMS_CHECK_SC( sc, "EMMC: CMD8 error");

    int f8_flag = 0;

    if (sc == RTEMS_TIMEOUT)
      f8_flag = 0;

    else if (sc == RTEMS_UNSATISFIED)
    {
        printk ("EMMC: failure sending CMD8 (%08x)\n", e->last_interrupt);
        return RTEMS_UNSATISFIED;
    }

    else
    {
        if ((e->last_r0 & 0xfff) != 0x1AA)
        {
            printk ("EMMC: unusable card\n");

            if (e->verbose)
                printk("EMMC: CMD8 response %08x\n", e->last_r0);

            return RTEMS_UNSATISFIED;
        }

        else
          f8_flag = 1;
    }

    /* Here we are supposed to check the response to CMD5 (HCSS 3.6 - step 5)
     * It only returns if the card is a SDIO card
     */
    if (e->verbose)
        printk("EMMC: note that a timeout error on the following command (CMD5) is "
               "normal and expected if the card is not a SDIO card.\n");

    sc = sd_issue_command (e, SD_CMD_IO_SET_OP_COND, 0, 10000);

    if ( sc == RTEMS_SUCCESSFUL)
    {
        printk ("EMMC: SDIO card detected - not currently supported\n");

        if (e->verbose)
	    printk("EMMC: CMD5 returned %08x\n", e->last_r0);

        return RTEMS_UNSATISFIED;
    }

    /* 
     * Call an inquiry ACMD41 (voltage window = 0) to get the OCR 
     * (HCSS 3.6 - step 11)
     */

    if (e->verbose)
        printk("EMMC: sending CMD55\n");

    sc = sd_issue_command_int (e, SD_CMD_APP_CMD, e->card_rca << 16, 500000);
    RTEMS_CHECK_SC( sc, "EMMC: CMD55 error");

    if (e->verbose)
        printk("EMMC: sending inquiry ACMD41\n");

    sc = sd_issue_command (e, APP_CMD_SEND_OP_COND, 0, 500000);

    if (sc != RTEMS_SUCCESSFUL)
    {
        printk ("EMMC: inquiry ACMD41 failed\n");
        return RTEMS_UNSATISFIED;
    }

    if (e->verbose)
        printk("EMMC: inquiry ACMD41 returned %08x\n", e->last_r0);

    /* 
     * Call initialization ACMD41 
     */

    int card_is_busy = 1;

    while (card_is_busy)
    {
        uint32_t SDHC_flag = 0;

        if (f8_flag)
        {
            /* Set SDHC support */
	    SDHC_flag |= (1 << 30);
        }

	if (e->verbose)
            printk("EMMC: sending CMD55\n");

	sc = sd_issue_command_int (e, SD_CMD_APP_CMD, e->card_rca << 16, 500000);
	RTEMS_CHECK_SC( sc, "EMMC: CMD55 error");

        sd_issue_command (e, APP_CMD_SEND_OP_COND, 0x00ff8000 | SDHC_flag, 500000);

        if (sc != RTEMS_SUCCESSFUL)
        {
            printk ("EMMC: error issuing ACMD41\n");
            return RTEMS_UNSATISFIED;
        }

        if ((e->last_r0 >> 31) & 0x1)
        {
            /* Initialization is complete */
            e->card_ocr = (e->last_r0 >> 8) & 0xffff;
            e->card_supports_sdhc = (e->last_r0 >> 30) & 0x1;

            card_is_busy = 0;
        }

        else
        {
            /* Card is still busy */
            if (e->verbose)
                printk("EMMC: card is busy, retrying\n");

            usleep (500000);
        }
    }

    if (e->verbose)
        printk("EMMC: card identified: OCR: %04x, SDHC support: %i\n",
               e->card_ocr, e->card_supports_sdhc);
    
    /* 
     * At this point, we know the card is definitely an SD card, so will definitely
     * support SDR12 mode which runs at 25 MHz
     */

    sd_switch_clock_rate (e, base_clock, SD_CLOCK_NORMAL);

    /* Send CMD2 to get the cards CID */
    sc = sd_issue_command (e, SD_CMD_ALL_SEND_CID, 0, 500000);

    if (sc != RTEMS_SUCCESSFUL)
    {
        printk("EMMC: error sending SD_CMD_ALL_SEND_CID\n");
        return RTEMS_UNSATISFIED;
    }

    uint32_t card_cid_0 = e->last_r0;
    uint32_t card_cid_1 = e->last_r1;
    uint32_t card_cid_2 = e->last_r2;
    uint32_t card_cid_3 = e->last_r3;

    if (e->verbose)
        printk("EMMC: card CID: %08x%08x%08x%08x\n", card_cid_3, card_cid_2, card_cid_1, card_cid_0);

    /* Send CMD3 to enter the data state */
    sc = sd_issue_command (e, SD_CMD_SEND_RELATIVE_ADDR, 0, 500000);

    if (sc != RTEMS_SUCCESSFUL)
    {
        printk ("EMMC: error sending SD_CMD_SEND_RELATIVE_ADDR\n");

        return RTEMS_UNSATISFIED;
    }

    uint32_t cmd3_resp = e->last_r0;

    if (e->verbose)
        printk("EMMC: CMD3 response: %08x\n", cmd3_resp);

    e->card_rca = (cmd3_resp >> 16) & 0xffff;

    uint32_t crc_error = (cmd3_resp >> 15) & 0x1;
    uint32_t illegal_cmd = (cmd3_resp >> 14) & 0x1;
    uint32_t error = (cmd3_resp >> 13) & 0x1;
    uint32_t status = (cmd3_resp >> 9) & 0xf;
    uint32_t ready = (cmd3_resp >> 8) & 0x1;

    if (crc_error)
    {
        printk ("EMMC: CRC error\n");

        return RTEMS_UNSATISFIED;
    }

    if (illegal_cmd)
    {
        printk ("EMMC: illegal command\n");
  
        return RTEMS_UNSATISFIED;
    }

    if (error)
    {
        printk ("EMMC: generic error\n");

        return RTEMS_UNSATISFIED;
    }

    if (!ready)
    {
        printk ("EMMC: not ready for data\n");

        return RTEMS_UNSATISFIED;
    }

    if (e->verbose)
        printk("EMMC: RCA: %04x\n", e->card_rca);

    /* Get the CSD structure from the card */
    sc = sd_issue_command (e, SD_CMD_SEND_CSD, e->card_rca << 16, 500000);
    
    if (sc != RTEMS_SUCCESSFUL)
    {
        printk("EMMC: error sending SD_CMD_SEND_CSD\n");
        return RTEMS_UNSATISFIED;
    }
    
    uint32_t card_csd_0 = e->last_r0;
    uint32_t card_csd_1 = e->last_r1;
    uint32_t card_csd_2 = e->last_r2;
    uint32_t card_csd_3 = e->last_r3;

    if (e->verbose)
        printk("EMMC: card CSD: %08x | %08x | %08x | %08x\n", card_csd_3, card_csd_2, card_csd_1, card_csd_0);

    uint8_t csd_structure = 0;
    uint32_t read_block_size = 0;
    uint32_t write_block_size = 0;

    csd_structure = card_csd_3 >> 30 - CSD_OFFSET;

    if (csd_structure == 1) 
    {
        uint32_t c_size = ((card_csd_1 >> 16 - CSD_OFFSET) | ((card_csd_2 & 0x2F) << 0xF ));

        /* Block size is fixed in CSD Version 2.0 */
        e->block_size_shift = 9;
        e->block_size = 512;

        e->block_number = (c_size + 1) * 1024;

        read_block_size = 512;
        write_block_size = 512;
    }

    else 
    {
        printk ("EMMC: card CSD_struct version is not supported\n");

        return RTEMS_UNSATISFIED;
    }

    if (e->verbose)
        printk("EMMC: block size %d: block number %d: capacity: %d MB\n", e->block_size, e->block_number, e->block_number/(uint32_t)2048);

    /* Now select the card (toggles it to transfer state) */
    sc = sd_issue_command (e, SD_CMD_SELECT_DESELECT_CARD, e->card_rca << 16, 500000);

    if (sc != RTEMS_SUCCESSFUL)
    {
        printk ("EMMC: error sending CMD7\n");

        return RTEMS_UNSATISFIED;
    }

    uint32_t cmd7_resp = e->last_r0;
    status = (cmd7_resp >> 9) & 0xf;

    if ((status != 3) && (status != 4))
    {
        printk ("EMMC: invalid status (%i)\n", status);

        return RTEMS_UNSATISFIED;
    }

    /* If not an SDHC card, ensure BLOCKLEN is 512 bytes */
    if (!e->card_supports_sdhc)
    {
        sc = sd_issue_command (e, SD_CMD_SET_BLOCKLEN, 512, 500000);

        if (sc != RTEMS_SUCCESSFUL)
        {
            printk ("EMMC: error sending SD_CMD_SET_BLOCKLEN\n");

            return RTEMS_UNSATISFIED;
        }
    }

    e->block_size = 512;

    uint32_t controller_block_size = BCM2835_REG (BCM2835_EMMC_BLKSIZECNT);
    controller_block_size &= (~0xfff);
    controller_block_size |= 0x200;

    BCM2835_REG (BCM2835_EMMC_BLKSIZECNT) = controller_block_size;

    if (e->verbose)
        printk("EMMC: setup successful\n");
        
    /* Reset interrupt register */
    BCM2835_REG (BCM2835_EMMC_INTERRUPT) = 0xffffffff;

    return RTEMS_SUCCESSFUL;
}

static int sd_ensure_data_mode ( struct emmc_block_dev *e )
{
    rtems_status_code sc;

    if (e->card_rca == 0)
    {
        /* Try again to initialise the card */
        int ret = sd_card_init ( e );

        if(ret != 0)
            return ret;
    }

    if (e->verbose)
        printk ("EMMC: ensure_data_mode() obtaining status register for card_rca %08x: \n",
                e->card_rca);

    sc = sd_issue_command (e, SD_CMD_SEND_STATUS, e->card_rca << 16, 500000);

    if (sc != RTEMS_SUCCESSFUL)
    {
        printk ("EMMC: ensure_data_mode() error sending CMD13\n");
        e->card_rca = 0;

        return -1;
    }

    uint32_t status = e->last_r0;
    uint32_t cur_state = (status >> 9) & 0xf;

    if (e->verbose)
        printk ("status %i\n", cur_state);

    if (cur_state == 3)
    {
        /* Currently in the stand-by state - select it */
        sc = sd_issue_command (e, SD_CMD_SELECT_DESELECT_CARD, e->card_rca << 16, 500000);
            
        if (sc != RTEMS_SUCCESSFUL)
        {
            printk ("EMMC: ensure_data_mode() no response from CMD17\n");
            e->card_rca = 0;
                
            return -1;
        }
    }

    else if (cur_state == 5)
    {
        /* In the data transfer state - cancel the transmission */
        sc = sd_issue_command (e, SD_CMD_STOP_TRANSMISSION, 0, 500000);
        
        if(sc != RTEMS_SUCCESSFUL)
        {
            printk ("EMMC: ensure_data_mode() no response from CMD12\n");
            e->card_rca = 0;
            
            return -1;
        }

        /* Reset the data circuit */
        sd_reset_dat ();
    }

    else if (cur_state != 4)
    {
        /* Not in the transfer state - re-initialise */
        int ret = sd_card_init ( e );

        if(ret != 0)
            return ret;
    }

    /* Check again that we're now in the correct mode */
    if (cur_state != 4)
    {
        if (e->verbose)
            printk ("EMMC: ensure_data_mode() rechecking status: ");

        sc = sd_issue_command (e, SD_CMD_SEND_STATUS, e->card_rca << 16, 500000);

        if (sc != RTEMS_SUCCESSFUL)
        {
            printk ("EMMC: ensure_data_mode() no response from CMD13\n");
            e->card_rca = 0;
                        
            return -1;
        }
        
        status = e->last_r0;
        cur_state = (status >> 9) & 0xf;

        if (e->verbose)
            printk ("%i\n", cur_state);

        if(cur_state != 4)
        {
            printk ("EMMC: unable to initialise SD card to "
                    "data mode (state %i)\n", cur_state);
                     e->card_rca = 0;
        
            return -1;
        }
    }

    return 0;
}

static int sd_do_data_command ( struct emmc_block_dev *e, int is_write, uint8_t *buf, size_t buf_size, uint32_t block_no )
{
    rtems_status_code sc;
  
    /* PLSS table 4.20 - SDSC cards use byte addresses rather than block addresses */
    if (!e->card_supports_sdhc)
        block_no *= 512;

    /* This is as per HCSS 3.7.2.1 */
    if (buf_size < e->block_size)
    {
        printk ("EMMC: do_data_command() called with buffer size (%i) less than "
                "block size (%i)\n", buf_size, e->block_size);

        return -1;
    }

    e->blocks_to_transfer = buf_size / e->block_size;

    if (e->verbose)
        printk ("\nEMMC: blocks to transfer -> %d\n", e->blocks_to_transfer);

    if (buf_size % e->block_size)
    {
        printk ("EMMC: do_data_command() called with buffer size (%i) not an "
                 "exact multiple of block size (%i)\n", buf_size, e->block_size);

        return -1;
    }
        
    e->buf = buf;

    /* Decide on the command to use */
    enum commands command;
     
    if (is_write)
    {
        if (e->blocks_to_transfer > 1)
            command = SD_CMD_WRITE_MULTIPLE_BLOCK;

        else
            command = SD_CMD_WRITE_BLOCK;
    }
     
    else
    {
        if (e->blocks_to_transfer > 1)
            command = SD_CMD_READ_MULTIPLE_BLOCK;

        else
	  {
            command = SD_CMD_READ_SINGLE_BLOCK;
	  }
    }

    int retry_count = 0;
    int max_retries = 3;

    while (retry_count < max_retries)
    {
        sc = sd_issue_command (e, command, block_no, 5000000);

        if (sc == RTEMS_SUCCESSFUL)
            break;

        else
        {
            printk ("EMMC: error sending CMD%i, ", command);
            printk ("error = %08x.  ", e->last_error);

            retry_count++;

            if (retry_count < max_retries)
                printk ("Retrying...\n");

            else
                printk ("Giving up.\n");
        }
    }

    if (retry_count == max_retries)
    {
        e->card_rca = 0;
        return -1;
    }

    return 0;
}

int sd_read ( struct emmc_block_dev *e, uint8_t *buf, size_t buf_size, uint32_t block_no )
{
    /* Check the status of the card */
    if (sd_ensure_data_mode(e) != 0)
        return -1;

    if (e->verbose)
        printk ("EMMC: read() card ready, reading from block %u\n", block_no);

    if (sd_do_data_command ( e, 0, buf, buf_size, block_no) < 0 )
        return -1;

    if (e->verbose)
        printk ("EMMC: data read successful\n");

    return buf_size;
}

int sd_write ( struct emmc_block_dev *e, uint8_t *buf, size_t buf_size, uint32_t block_no )
{
    /* Check the status of the card */
    if (sd_ensure_data_mode (e) != 0)
        return -1;

    if (e->verbose)
        printk ("EMMC: write() card ready, reading from block %u\n", block_no);

    if (sd_do_data_command (e, 1, buf, buf_size, block_no) < 0)
        return -1;

    if (e->verbose)
        printk ("EMMC: write successful\n");

    return buf_size;
}

rtems_device_driver sd_card_disk_init ( rtems_device_major_number major
                                      , rtems_device_minor_number minor
                                      , void *arg )
{
    rtems_status_code sc = RTEMS_SUCCESSFUL;
    dev_t dev;
    struct emmc_block_dev *e;

    /* Initialize disk IO */
    sc = rtems_disk_io_initialize ();
    RTEMS_CHECK_SC( sc, "Initialize RTEMS disk IO" );

    dev = rtems_filesystem_make_dev_t ( major, minor );

    /* Prepare the device structure */
    e = &sd_card_driver_table [ minor ];

    e->major = major;
    e->minor = minor;

    e->verbose = false;

    /* Initialize SD Card */
    sc = sd_card_init ( e );
    RTEMS_CHECK_SC ( sc, "Initialize SD Card" );

    /* Create disk device */
    sc = rtems_disk_create_phys ( dev, e->block_size, e->block_number, disk_ioctl, NULL, "/dev/sd-card" );
    RTEMS_CHECK_SC ( sc, "Create disk device" );

    return RTEMS_SUCCESSFUL;
}
