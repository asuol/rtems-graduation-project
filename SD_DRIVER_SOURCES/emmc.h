#include <rtems.h>
#include <rtems/blkdev.h>

#include <stdbool.h>

rtems_device_driver sd_card_disk_init(rtems_device_major_number majr,
    rtems_device_minor_number minor,
    void *arg);

struct emmc_block_dev
{
    /* card type */
    bool card_supports_sdhc;

    uint32_t card_ocr;
    uint32_t card_rca;

    uint32_t last_interrupt;
    uint32_t last_error;
    uint32_t last_cmd_reg;
    uint32_t last_cmd;
    uint32_t last_cmd_success;
    uint32_t last_r0;
    uint32_t last_r1;
    uint32_t last_r2;
    uint32_t last_r3;

    void *buf;
    int blocks_to_transfer;

    int card_removal;
    uint32_t base_clock;

    uint32_t block_number;
    uint32_t block_size;
    uint32_t block_size_shift;

    bool verbose;

    rtems_device_major_number major;
    rtems_device_minor_number minor;
};

int sd_read (struct emmc_block_dev *e, uint8_t *buf, size_t buf_size, uint32_t block_no);
int sd_write (struct emmc_block_dev *e, uint8_t *buf, size_t buf_size, uint32_t block_no);

#define SD_CARD_DRIVER_TABLE_ENTRY \
  { sd_card_disk_init, \
    rtems_blkdev_generic_open, \
    rtems_blkdev_generic_close, \
    rtems_blkdev_generic_read, \
    rtems_blkdev_generic_write,	\
    rtems_blkdev_generic_ioctl \
  }
