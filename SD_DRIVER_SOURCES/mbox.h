/* 
 * References:
 *
 * https://github.com/raspberrypi/firmware/wiki/Mailboxes
 *
 * https://github.com/raspberrypi/firmware/wiki/Accessing-mailboxes
 */

#ifndef MBOX_H
#define MBOX_H

uint32_t mbox_read ( uint8_t channel );
void mbox_write ( uint8_t channel, uint32_t data );

#endif /* MBOX_H */
